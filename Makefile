PREFIX		=  
AR		=  $(PREFIX)ar
CC		=  $(PREFIX)gcc
CFLAGS		=  -c -Wall -O2 -g
LDFLAGS		=  
SOURCEDIR	=  ./
CSOURCES	=  $(foreach dir,$(SOURCEDIR),$(notdir $(wildcard $(dir)/*.c)))
OBJECTS		=  $(CSOURCES:.c=.o)
PRODUCT		=  update_downloader

all: $(PRODUCT)

install: $(PRODUCT)
	cp $(PRODUCT) /usr/bin/$(PRODUCT)

%.a: $(OBJECTS)
	$(AR) rcs $@ $(OBJECTS)

$(PRODUCT): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) $(OBJECTS) $(PRODUCT)

